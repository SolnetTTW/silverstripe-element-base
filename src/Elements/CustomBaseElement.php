<?php

namespace Solnet\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\ValidationResult;

/**
 * Base element used by Solnet projects using DNA's Elemental module.
 */

class CustomBaseElement extends BaseElement
{
    use Configurable;

    private static $table_name = 'CustomBaseElement';
    private static $singular_name = 'Custom Base';
    private static $plural_name = 'Custom Base';
    private static $description = 'Custom Base Module';

    private static $db = [
        'Anchor' => 'Varchar'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // Enable setting different title labels on different subclasses
        $fields->fieldByName('Root.Main.TitleAndDisplayed')
        ->setTitle(
            _t(get_class($this) . '.TitleLabel', 'Title (displayed if checked)')
        );

        $fields->addFieldsToTab('Root.Settings', array(
            $anchor = TextField::create('Anchor', _t('Element.SolnetBase_Anchor_Title', 'Anchor')),
        ));

        $anchor->setDescription(
            _t(
                'Element.SolnetBase_Anchor_Description',
                'Can be used in the URL to link directly to this point in the page.'
            ) .
            '<br>' .
            _t(
                'Element.SolnetBase_Anchor_ValidationError',
                'Use only letters, numbers, and underscores; no spaces.'
            )
        );

        $fields->removeByName('ExtraClass');

        return $fields;
    }

    /**
     * Enforces rules on the Anchor field to only accept numbers, letters, hyphen and underscore.
     *
     * @return SilverStripe\ORM\ValidationResult
     */
    public function validate()
    {
        $result = parent::validate();

        if ($this->isChanged('Anchor')) {
            if (!preg_match("/^[A-Za-z0-9_]+$/", $this->getField('Anchor'))) {
                $result->addFieldError(
                    'Anchor',
                    _t(
                        'Element.SolnetBase_Anchor_ValidationError',
                        'Use only letters, numbers, and underscores; no spaces.'
                    ),
                    ValidationResult::TYPE_ERROR
                );
            }
        }

        return $result;
    }
}
